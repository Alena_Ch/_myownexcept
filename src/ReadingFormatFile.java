import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class ReadingFormatFile {
    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("test");

        Scanner sc = new Scanner(file);
        String line = sc.nextLine();
        String[] numbers = line.split(" ");
        int[] num = new int[3];
        int counter = 0;

        for (String number : numbers) {
            num[counter++] = Integer.parseInt(number);
        }

        System.out.println(Arrays.toString(numbers));
        sc.close();
    }
}
